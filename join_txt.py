import os,codecs

def join_it(filx,Fo):
    print filx
    Fi=codecs.open(filx,'r','utf-8')
    while 1:
        T=Fi.read(100000)
        if T:
            Fo.write(T)
        else:
            break
    Fi.close()

if __name__=="__main__":
    filxs=os.listdir('.')
    Fo=codecs.open('output.corpus','w','utf-8')

    for filx in filxs:
        if filx.endswith('.txt') and filx not in ['rooms.txt','requirements.txt']:
            join_it(filx,Fo)
    Fo.close()
    print 'Done'