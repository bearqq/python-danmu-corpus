# coding: utf-8

#some definitions:

#imports
import sys
import codecs
import threading
import time
import rooms
import copy

if sys.getdefaultencoding() != 'utf8':
	reload(sys)
	sys.setdefaultencoding( "utf-8" )

class tick():
	"""
	def done(min,sec):
		print min,sec
	
	tk=tick(done,10000)
	while 1:
		tk.tick()
	"""
	def __init__(self,done,loop=1000):
		self.loop=loop
		self.sec=self.loop
		self.done=done
		self.min=0

	def tick(self):
		self.sec=self.sec-1
		if self.sec<1:
			self.min=self.min+1
			self.sec=self.loop
			self.done(self.min,self.loop)
"""
def pp(msg):
    print(msg.encode(sys.stdin.encoding, 'ignore').
        decode(sys.stdin.encoding))
"""

def dm_handle(url):
    from danmu import DanMuClient

    f_name='.'.join(url.split('/')[2:]+['txt'])
    file_out=codecs.open(f_name,'a','utf-8')
    try:
        dmc = DanMuClient(url)

        if not dmc.isValid():
            print 'Url not valid: ',url
            sys.exit()
        
        def done(min,sec):
            print 'Done 100@url: ',url
        tk=tick(done,100)

        #@dmc.danmu
        def danmu_fn(msg):
            if msg['Content'].startswith(u'感谢') or msg['Content'].startswith(u'6666666') or msg['Content'].startswith(u'欢迎'):
                return
            tk.tick()
            file_out.write(msg['Content'])
            file_out.write('\n')
            #pp('[%s] %s' % (msg['NickName'], msg['Content']))
        dmc.danmu(danmu_fn)


        dmc.start(blockThread = False)
        time.sleep(60*60)
        dmc.stop()
        file_out.close()
        return
        #sys.exit()
    except Exception as e:
        print 'Error for url:%s \r\n\t%s' %(url,str(e))
        return
        #time.sleep(60)

if __name__ == '__main__':
    urls=[
        'http://www.douyu.com/214786',#两仪未娜丶
        'http://www.panda.tv/79289',#太极剑
        'http://www.douyu.com/xiaoruoji',#可爱的小弱鸡
        'http://www.panda.tv/act/blizzardtv20160510.html',#blizzard
        'http://www.douyu.com/525207',#blizzard
        'http://www.zhanqi.tv/topic/baoxuetv',#blizzard
        #'http://www.huya.com/a16789',#安德罗妮丶
        'http://www.douyu.com/268025',#低调的河马
        'http://www.douyu.com/kejimeixue',#科技美学中国
        'http://www.douyu.com/85513',#肥皂侠
        'http://www.douyu.com/taidusky',#钛度Sky
        'http://www.douyu.com/600878',#炉石丶贪睡之萨满
        'http://www.douyu.com/TED615',#ForeverTED
        'http://www.douyu.com/gouzei',#恩基爱萌萌哒的狗贼
    ]

    while 1:
        urls_dy=rooms.main(append_text_rooms=0)
        urls2=copy.copy(urls)
        for url in urls_dy.keys():
            urls2.append('http://www.douyu.com%s' % url)

        """
        threads=[]
        
        for url in urls2:
            t=threading.Thread(target=dm_handle,args=(url,))
            threads.append(t)
        for t in threads:
            t.start()
        print "Danmu init done."
        for t in threads:
            t.join()
        """
        from multiprocessing import Pool	#multi_process
        pool = Pool(len(urls2))
        pool.map(dm_handle,urls2)
        pool.close()
        print "Danmu init done."
        pool.join()
        





"""
if __name__ == '__main__':
    urls=[
        'http://www.douyu.com/214786',#两仪未娜丶
        'http://www.panda.tv/79289',#太极剑
        'http://www.douyu.com/xiaoruoji',#可爱的小弱鸡
        'http://www.panda.tv/act/blizzardtv20160510.html',#blizzard
        'http://www.douyu.com/525207',#blizzard
        'http://www.zhanqi.tv/topic/baoxuetv',#blizzard
        #'http://www.huya.com/a16789',#安德罗妮丶
        'http://www.douyu.com/268025',#低调的河马
        'http://www.douyu.com/kejimeixue',#科技美学中国
        'http://www.douyu.com/85513',#肥皂侠
        'http://www.douyu.com/taidusky',#钛度Sky
        'http://www.douyu.com/600878',#炉石丶贪睡之萨满
        'http://www.douyu.com/TED615',#ForeverTED
        'http://www.douyu.com/gouzei',#恩基爱萌萌哒的狗贼
    ]

    threads=[]
    for url in urls:
        t=threading.Thread(target=dm_handle,args=(url,))
        threads.append(t)
    for t in threads:
        t.start()
    print "Danmu init done."
    for t in threads:
        t.join()
"""


"""
@dmc.gift
def gift_fn(msg):
    pp('[%s] sent a gift!' % content['NickName'])

@dmc.other
def other_fn(msg):
    pp('Other message received')
"""
