# coding: utf-8

import requests
import sys
from bs4 import BeautifulSoup
import codecs
import json

if sys.getdefaultencoding() != 'utf8':
	reload(sys)
	sys.setdefaultencoding( "utf-8" )

def main(append_text_rooms=0):
    req=requests.get('http://www.douyu.com/#')

    soup = BeautifulSoup(req.content,'lxml')#,from_coding="GB18030"
    lis=soup.find_all("li")#, attrs={"class": " "}

    if append_text_rooms:
        try:
            F=codecs.open('rooms.txt','r','utf-8')
            l=F.readline()
            links=json.loads(l.strip())
            F.close()
        except:
            print 'read links error'
            links={}
    else:
        links={}

    for li in lis:
        try:
            #print li.a.attrs['title'],li.a.attrs['href'],li.a.find_all('span',attrs={'class':'dy-name ellipsis fl'})[0].text
            #links.append((li.a.attrs['title'],li.a.attrs['href'],li.a.find_all('span',attrs={'class':'dy-name ellipsis fl'})[0].text))
            links[li.a.attrs['href']]=(li.a.attrs['title'],li.a.find_all('span',attrs={'class':'dy-name ellipsis fl'})[0].text)
        except:
            pass

    F=codecs.open('rooms.txt','w','utf-8')
    F.write(json.dumps(links))
    F.write('\r\n')
    for i in links.keys():
        F.write('"http://www.douyu.com%s",#%s %s\r\n' % (i,links[i][0],links[i][1]))
        print links[i][0].encode('gb18030'),links[i][1].encode('gb18030')
    F.close()
    return links

if __name__ == '__main__':
    main(1)